import glob
import json
import os
import requests

from bs4 import BeautifulSoup as bs
from pprint import pprint
from requests.adapters import Response

page = 0
base_url = 'https://hh.ru/search/vacancy'
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36',
    'Accept': '*/*'
}


def create_filter() -> dict:
    """Создает фильтр, по которому будет осуществляться поиск"""
    salary = int(input('Введите желаемую зарплату: '))
    experience = int(input('Введите свой опыт работы, если его нет введите 0: '))
    employment = int(input('Полная занятость(1), Частичноя занятость(2) или стажировка(3): '))
    schedule = int(input('Удаленнная работа(1) или офисный день(2): '))

    filter = {} 

    filter['salary'] = salary

    # Установка Experience
    if experience <= 3 and experience > 0:
        filter['experience'] = 'between1And3'
    elif experience > 3 and experience <= 6:
        filter['experience'] = 'between3And6'
    elif experience > 6:
        filter['experience'] = 'moreThan6'
    elif experience == 0: 
        filter['experience'] = 'noExperience'

    # Установка занятости
    if employment == 1:
        filter['employment'] = 'full'
    elif employment == 2:
        filter['employment'] = 'part'
    elif employment == 3:
        filter['employement'] = 'probation'

    #Установка расписания
    if schedule == 1:
        filter['schedule'] = 'remote'
    elif schedule == 2:
        filter['schedule'] = 'fullDay'

    return filter

def get_url() -> str:
    """Создаем ссылку для каждой страницы запроса"""
    params = {
        'area': 1,
        'page': page,
        'text': search_text,
        'enable_snippets': 'true',
        'clusters': True
    }
    url = f"{base_url}?area={params['area']}&enable_snippets={params['enable_snippets']}&clusters={params['clusters']}&text={params['text']}&page={params['page']}"
    return url

def get_url_with_filter(filter: dict) -> str:
    """Создаем ссылку для каждой страницы с приминением фильтра"""
    params = {
        'area': 1,
        'page': page,
        'text': search_text,
        'clusters': True,
        'enable_snippets': 'true',
        'filter': filter
    }
    url = f"{base_url}?area={params['area']}&enable_snippets={params['enable_snippets']}&clusters={params['clusters']}&text={params['text']}&page={params['page']}&salary={params['filter']['salary']}&experience={params['filter']['experience']}&employment={params['filter']['employment']}&schedule={params['filter']['schedule']}"
    return url
    
def get_response(url: str) -> Response:
    """Получаем Response из приходящей ссылки"""
    response = requests.get(url=url, headers=headers) 
    return response

def get_max_page(response: Response) -> int:
    """Получаем максимальное число страниц по данному запросу"""
    soup = bs(response.content, 'html.parser')
    result = 0
    pager_controls = soup.find_all('a')
    for a in pager_controls:
        if (a.get('class') == ['bloko-button', 'HH-Pager-Control']):
            arr = a.text.split()
            result = max(arr)
    return result

def page_parse(url: str):
    """Получаем ключевую информацию о каждой вакансии"""
    response = get_response(url)
    soup = bs(response.content, 'html.parser')
    link_modifier = soup.find_all('a')
    vacancy_list = []
    for a in link_modifier:
        if(a.get('class') == ['bloko-link', 'HH-LinkModifier']):
            vacancy = {
                'title': a.text,
                'link': a.get('href')
            }
            vacancy_list.append(vacancy)
            create_json_list(vacancy_list)

def create_json_list(vacancy: list):
    """Создает файлы в папке json, которые содержут информацию о вакансиях"""
    try:
        with open(f'json/vacancy_{page + 1}.json', 'w', encoding='utf8') as json_file:
            json.dump(vacancy, json_file, ensure_ascii=False)
    except FileNotFoundError:
        os.mkdir('json')
        create_json_list(vacancy)

def get_page_count(use_filter: str) -> int:
    """Получает количество страниц"""
    response = get_response(get_url())
    if use_filter == 'True':
        response = get_response(get_url_with_filter(filter=filter))
    page_count = int(get_max_page(response=response))
    return page_count

def delete_json_files():
    """Удаляет все содержимое в папке json"""
    files = glob.glob('json/*')
    for file in files:
        os.remove(file)

def rebuild_search_text(start_text: str) -> str:
    """Меняет несколько символов, чтобы запрос воспринимался как должен"""
    result_text = start_text.replace('+', '%2B') # C++ support
    result_text = result_text.replace('#', '%23') # C# support

    search_list = result_text.split()
    search_count = len(search_list)
    if search_count > 1:
        result_text = ''
        for word in search_list:
            result_text += word
            if search_list[search_count - 1] == word:
                break
            result_text += '+'
    return result_text

def create_search_query() -> str:
    """Создает переменную, которая будет хранить вакансию, которую мы ищем"""
    search_text = input('Введите требуемую вакансию: ')
    url_text = rebuild_search_text(search_text)
    return url_text

search_text = create_search_query()

filter = None
use_filter = input('Нужно ли использовать фильтр по вакансиям(True or False)?: ')
if use_filter == 'True':
        filter = create_filter()

max_page = get_page_count(use_filter)
delete_json_files()

while(page < max_page):
    url = get_url()
    if use_filter == 'True':
        url = get_url_with_filter(filter=filter)
    page_parse(url)
    print(f'Page {page + 1}/{max_page}')
    page += 1